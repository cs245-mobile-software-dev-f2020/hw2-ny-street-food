package com.example.hw2

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import kotlinx.android.synthetic.main.fragment_detail.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [DetailFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class DetailFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detail, container, false)
    }

    val viewModel: RestaurantViewModel by activityViewModels<RestaurantViewModel>()
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.currentRestaurant.observe(viewLifecycleOwner, {
            banner_imageView.setImageResource(
                when (it.style) {
                    "American" -> R.drawable.food_american
                    "Chinese" -> R.drawable.food_chinese
                    "Dessert" -> R.drawable.food_dessert
                    "Fast Food" -> R.drawable.food_fastfood
                    "French" -> R.drawable.food_french
                    "Italian" -> R.drawable.food_italian
                    "Japanese" -> R.drawable.food_japanese
                    "Mexican" -> R.drawable.food_mexican
                    "Pizza" -> R.drawable.food_pizza
                    "Steakhouse" -> R.drawable.food_steakhouse
                    else -> R.drawable.food_american
                }
            )
            name_text.text = it.name
            style_text.text = it.style
            address_text.text = it.address
            ratingBar.rating = it.rating
            review_text.text = it.comment
        })
        edit_button.setOnClickListener {
            findNavController().navigate(R.id.action_detailFragment_to_reviewFragment)
        }
        ratingBar.setOnRatingBarChangeListener { ratingBar, fl, b ->
            viewModel.currentRestaurant.value?.rating = fl
        }
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment DetailFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            DetailFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}