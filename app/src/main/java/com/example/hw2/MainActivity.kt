package com.example.hw2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import androidx.activity.viewModels
import androidx.navigation.fragment.NavHostFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    val viewModel: RestaurantViewModel by viewModels<RestaurantViewModel>()

    /**
     * read the CSV data
     * */
    fun loadData() {
        val dataString =
            resources.openRawResource(R.raw.data).bufferedReader()
                .use { it.readText() }// read the entire file as a string
        var lines = dataString.trim().split("\n") // split each line
        lines = lines.subList(1, lines.size) // get rid of the header line
        //Add to the stock Array.
        lines.forEach {
            viewModel.addRestaurant(makeRestaurant(it))
        }
    }

    /**
     * Split one linfe from CSV file and create the stock object
     * @param line : one line of values from the csv file. e.g. "Industrials,3M Company,222.89,259.77,175.49"
     */
    fun makeRestaurant(line: String): Restaurant {
        val cells = line.split(",")
        val stock = Restaurant(
            cells[0],
            cells[1],
            cells[2],
            cells[3],
            cells[4],
            cells[5],
        )
        return stock
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (viewModel.restaurantList.value?.size == 0)
            loadData()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.home_menu -> {
                NavHostFragment.findNavController(nav_host_fragment)
                    .navigate(R.id.action_global_styleFragment)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}