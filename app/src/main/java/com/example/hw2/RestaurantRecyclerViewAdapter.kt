package com.example.hw2

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.fragment_detail.view.*
import kotlinx.android.synthetic.main.restaurant_list_item.view.*

class RestaurantRecyclerViewAdapter(
    var restaurantList: List<Restaurant>,
    val click: (Restaurant) -> Unit
) :
    RecyclerView.Adapter<RestaurantRecyclerViewAdapter.RecyclerViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewHolder {
        val viewItem = LayoutInflater.from(parent.context)
            .inflate(R.layout.restaurant_list_item, parent, false)
        return RecyclerViewHolder(viewItem)
    }

    override fun getItemCount(): Int {
        return restaurantList.size
    }

    override fun onBindViewHolder(holder: RecyclerViewHolder, position: Int) {
        holder.bind(restaurantList[position], click)
    }

    class RecyclerViewHolder(val viewItem: View) : RecyclerView.ViewHolder(viewItem) {
        fun bind(r: Restaurant, click: (Restaurant) -> Unit) {
            viewItem.rest_name.text = r.name
            viewItem.rest_info.text = r.service
            viewItem.rest_ratingbar.rating = r.rating
            viewItem.style_info.text = r.style
            viewItem.setOnClickListener {
                click(r)
            }
        }
    }
}