package com.example.hw2

data class Restaurant(
    val name: String,
    val service: String,
    val style: String,
    val address: String,
    val phone: String,
    val postcode: String,
) {
    var rating = 0.0f
    var comment = ""
}