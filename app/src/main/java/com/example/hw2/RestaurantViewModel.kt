package com.example.hw2

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class RestaurantViewModel : ViewModel() {
    val restaurantList = MutableLiveData<ArrayList<Restaurant>>()
    val currentRestaurant = MutableLiveData<Restaurant>()

    init {
        restaurantList.value = ArrayList<Restaurant>()
    }

    fun addRestaurant(res: Restaurant) {
        restaurantList.value?.add(res)
        currentRestaurant.value = restaurantList.value?.get(0)
        Log.d("mytest", "add restaurant")
    }
}