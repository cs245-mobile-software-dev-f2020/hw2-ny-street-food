package com.example.hw2

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import kotlinx.android.synthetic.main.fragment_style.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [StyleFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class StyleFragment : Fragment(), View.OnClickListener {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_style, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        american_button.setOnClickListener(this)
        chinese_button.setOnClickListener(this)
        dessert_button.setOnClickListener(this)
        fastfood_button.setOnClickListener(this)
        french_button.setOnClickListener(this)
        italian_button.setOnClickListener(this)
        japanese_button.setOnClickListener(this)
        mexican_button.setOnClickListener(this)
        pizza_button.setOnClickListener(this)
        steakhouse_button.setOnClickListener(this)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment StyleFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            StyleFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    override fun onClick(p0: View?) {
        val bundle = Bundle()
        when (p0?.id) {
            american_button.id -> bundle.putString("style", "American")
            chinese_button.id -> bundle.putString("style", "Chinese")
            dessert_button.id -> bundle.putString("style", "Dessert")
            fastfood_button.id -> bundle.putString("style", "Fast Food")
            french_button.id -> bundle.putString("style", "French")
            italian_button.id -> bundle.putString("style", "Italian")
            japanese_button.id -> bundle.putString("style", "Japanese")
            mexican_button.id -> bundle.putString("style", "Mexican")
            pizza_button.id -> bundle.putString("style", "Pizza")
            steakhouse_button.id -> bundle.putString("style", "Steakhouse")
        }
        findNavController().navigate(R.id.action_styleFragment_to_listFragment, bundle)
    }
}